# Andriago 
# Modelacion y Simulacion 4
#### Por Andrea Posada y Santiago Hincapie  
Querida Compañera este es nuestro Grupo de trabajo, en este encontraremos y alojaremos todos nuestros trabajos juntos, este repositorio alojará todo lo necesario que tenga que ver con modelacion y simulacion 4.

## Secciones
Las secciones que tenemos alojadas hasta el ultimo dia de actualizacion son las siguientes:

* Anteproyecto:
	- La ultima version del modelo realizado en simulink.
	- El escrito.
 * Bibliografia 
 	- Este posee un documento llamado "Libros y demas link.txt" que contiene la mayoria de los link que hemos usados hasta el momento.
 	- reference.bib el archivo *.bib que esta en construccion. 

## Algunos trucos para el manejo de git
Como ya sabes git es un sistemas de manejo de versiones a continuacion mensionaré alguno de los comandos mas utiles y comunes para el uso de git.

* git clone [direccion git]: Clona un repositorio alojado en git, para clonar este repositorio (por ejemplo) debes ejecutar "git clone git@bitbucket.org:andriago/modelacion-y-simulacion.git"
* git init: 
Con este comando creas un nuevo repositorio sobre un directorio creado, es decir esta orden se ejecuta sobre un directorio previamente creado en tu equipo personal.
* git remote add origin [server] :
Suponiendo que ya se posee un repositorio creado (como es el caso) de esta manera se conecta a la carpeta que acabas de iniciar.
* git add [filename] : 
registras los cambios realizados sobre un documento, cuando deseas subir un archivo por primera vez, tambien debes ejecutarlo.
* git commit -m "Comentario" [Filename]:
este comando ademas de comentar lo que realizamos, envia el archivo al head (en español ya casi esta subido).
* git push origin [rama] :
Este comando manda todos los cambios que esten por subir a la rama que se especifique, normalmente usarás "git push origin master".
 
 
 Si deseas saber mas git te recomiendo este [link](http://rogerdudler.github.io/git-guide/index.es.html) 